# pinephone-scripts

This is a collection of scripts for the PinePhone and other Linux phones. If you have any scripts that you think might be helpful to other Linux phone users, please create a pull request!

Scripts will be accepted under any license approved by the FSF or OSI. Please include license information in your script, along with your copyright notice and credit to anyone else who contributed to your code.